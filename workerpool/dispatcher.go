package workerpool

import (
	"log"
	"strconv"
)

type Dispatcher struct {
	WorkerPool chan chan *Job
}

var JobQueue chan *Job

func NewDispatcher(maxWorkers int) *Dispatcher {
	pool := make(chan chan *Job, maxWorkers)
	for i := 0; i < maxWorkers; i++ {
		worker := NewWorker(pool, strconv.Itoa(i))
		go worker.Start()
	}
	log.Println("Initialized Dispatcher")
	return &Dispatcher{WorkerPool: pool}
}

func (d *Dispatcher) Start() {
	log.Println("Dispatcher listening to Jobs")
	go func() {
		for {
			select {
			case job := <-JobQueue:
				go func(job *Job) {
					// This will block until a worker is available
					log.Println("Job " + job.Name() + " queued in Dispatcher")
					worker := <-d.WorkerPool
					worker <- job
				}(job)
			}
		}
	}()
}
