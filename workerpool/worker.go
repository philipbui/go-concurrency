package workerpool

import (
	"log"
)

type Worker struct {
	WorkerPool chan chan *Job
	WorkerJobs chan *Job
	quit       chan bool
	name       string
}

func NewWorker(workerPool chan chan *Job, name string) *Worker {
	return &Worker{
		WorkerPool: workerPool,
		WorkerJobs: make(chan *Job),
		quit:       make(chan bool),
		name:       name,
	}
}

func (w *Worker) Start() {
	log.Println("Worker " + w.name + " started")
	for {
		// Put worker back into Worker Pool
		w.WorkerPool <- w.WorkerJobs
		select {
		// Listening to the Job Channel
		case job := <-w.WorkerJobs:
			if job.ShouldExecute() {
				log.Println("Job " + job.Name() + " accepted by worker " + w.name)
				job.Execute()
			} else {
				//log.Println("Job " + job.Name() + " declined by worker " + w.name)
				JobQueue <- job // Ping PokemonGoServers later, this is put into the global JobQueue instead of worker JobQueue
			}
		// Listening to the Quit Channel
		case <-w.quit:
			return
		}
	}
}

func (w *Worker) Stop() {
	go func() {
		w.quit <- true
	}()
}
