package workerpool

import (
	"log"
	"pokehelper/service"
	"time"
)

type Job struct {
	Email     string
	Timestamp time.Time
}

func (job *Job) ShouldExecute() bool {
	if time.Since(job.Timestamp).Minutes() > 1 {
		return true
	}
	return false
}

func (job *Job) Execute() {
	job.Scan()
}

func (job *Job) Name() string {
	return job.Email
}
func (job *Job) Scan() {
	user, err := service.GetUser(job.Email)
	if err != nil {
		log.Println("Failed to find user for " + job.Email + ": " + err.Error())
		return
	}
	if err := service.Scan(user, 1); err != nil {
		log.Println("Failed to scan for user " + job.Email + ": " + err.Error())
		return
	}
	job.Timestamp = time.Now()
	JobQueue <- job
}
