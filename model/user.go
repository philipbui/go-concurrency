package model

import (
	"errors"

	"github.com/pkmngo-odi/pogo/api"
	"github.com/pkmngo-odi/pogo/auth"
)

type User struct {
	Email    string  "json:email"
	Password string  "json:password"
	Gcm      string  "json:gcm"
	Lat      float64 "json:lat"
	Lon      float64 "json:lng"
}

func (u *User) GetFeed() *api.Feed {
	return api.NewFeed(u.MakeReporter())
}

func (u *User) MakeReporter() *GcmReporter {
	return &GcmReporter{
		id: u.Gcm,
	}
}

func (u *User) GetSession() (*api.Session, error) {
	provider, err := u.GetProvider()
	if err != nil {
		return nil, err
	}
	session := api.NewSession(provider, u.GetLocation(), u.GetFeed(), true)
	session.Init()
	defer func() {
		if recover() != nil {
			err = errors.New("Unable to get playerInfo, check login")
		}
	}()
	return session, err
}

func (u *User) GetProvider() (auth.Provider, error) {
	return auth.NewProvider("google", u.Email, u.Password)
}

func (u *User) GetLocation() *api.Location {
	return &api.Location{
		Lat: u.Lat,
		Lon: u.Lon,
		Alt: 0.0,
	}
}
