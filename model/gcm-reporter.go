package model

import (
	"encoding/json"
	"fmt"

	_ "github.com/alexjlockwood/gcm"
	"github.com/pkmngo-odi/pogo-protos"
)

type GcmReporter struct {
	id string
}

func NewGcmReporter(id string) *GcmReporter {
	return &GcmReporter{
		id: id,
	}
}

func (r *GcmReporter) WildPokemons(pokemon []*protos.WildPokemon) {
	out, err := json.Marshal(pokemon)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(string(out))
	//gcm := gcm.NewMessage(nil, id)
}

func (r *GcmReporter) Forts(forts []*protos.FortData) {
	out, err := json.Marshal(forts)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(string(out))
}
