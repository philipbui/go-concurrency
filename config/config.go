package config

import (
	"flag"
)

var (
	Port         string
	RecheckTimer int
	Throttle     int
	Radius       float64
	GcmKey       string
)

func init() {
	flag.StringVar(&Port, "Port", ":8079", "Port for Server")
	flag.IntVar(&RecheckTimer, "RecheckTimer", 30, "Timer before rechecking for Pokemon again")
	flag.IntVar(&Throttle, "Throttle", 10, "Throttle Delay between each Scan")
	flag.Float64Var(&Radius, "Radius", 0.7, "Pulse Radius of Player Scan")

}
