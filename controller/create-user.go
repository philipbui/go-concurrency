package controller

import (
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"pokehelper/model"
	"pokehelper/route"
	"pokehelper/service"

	"github.com/julienschmidt/httprouter"
)

func CreateUser(w http.ResponseWriter, r *http.Request, _ httprouter.Params) *route.HttpError {
	var requestBody model.User
	if err := json.NewDecoder(io.LimitReader(r.Body, 10000)).Decode(&requestBody); err != nil {
		return &route.HttpError{err, "JSON length is over 10000 bytes", 422}
	}
	if requestBody.Email == "" {
		return &route.HttpError{errors.New("Could not find Email"), "Invalid Body", 422}
	}
	if requestBody.Password == "" {
		return &route.HttpError{errors.New("Could not find Password"), "Invalid Body", 422}
	}
	if requestBody.Lat == 0.0 {
		return &route.HttpError{errors.New("Lat could not be found"), "Invalid Body", 422}
	}
	if requestBody.Lon == 0.0 {
		return &route.HttpError{errors.New("Long could not be found"), "Invalid Body", 422}
	}
	if requestBody.Gcm == "" {
		return &route.HttpError{errors.New("Gcm could not be found"), "Invalid Body", 422}
	}
	if _, err := requestBody.GetSession(); err != nil {
		return &route.HttpError{err, "Could not validate session with API, check username and password", 500}
	}
	if err := service.CreateUser(&requestBody); err != nil {
		return &route.HttpError{err, "Could not save user to database", 500}
	}
	return nil
}
