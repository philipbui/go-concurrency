package controller

import (
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"pokehelper/model"
	"pokehelper/route"
	"pokehelper/service"

	"github.com/julienschmidt/httprouter"
)

func GetUser(w http.ResponseWriter, r *http.Request, _ httprouter.Params) *route.HttpError {
	var requestBody model.User
	if err := json.NewDecoder(io.LimitReader(r.Body, 10000)).Decode(&requestBody); err != nil {
		return &route.HttpError{err, "JSON length is over 10000 bytes", 422}
	}
	if requestBody.Email == "" {
		return &route.HttpError{errors.New("Email could not be found"), "Invalid Body", 422}
	}
	user, err := service.GetUser(requestBody.Email)
	if err != nil {
		return &route.HttpError{err, "Could not find user", 500}
	}
	json.NewEncoder(w).Encode(&user)
	return nil
}
