package controller

import (
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"pokehelper/model"
	"pokehelper/route"
	"pokehelper/service"

	"github.com/julienschmidt/httprouter"
)

func Scan(w http.ResponseWriter, r *http.Request, _ httprouter.Params) *route.HttpError {
	var requestBody model.User
	if err := json.NewDecoder(io.LimitReader(r.Body, 10000)).Decode(&requestBody); err != nil {
		return &route.HttpError{err, "JSON length is over 10000 bytes", 422}
	}
	if requestBody.Email == "" {
		return &route.HttpError{errors.New("Email could not be found"), "Invalid Body", 422}
	}
	if requestBody.Password == "" {
		return &route.HttpError{errors.New("Password could not be found"), "Invalid Body", 422}
	}
	if requestBody.Lat == 0.0 {
		return &route.HttpError{errors.New("Lat could not be found"), "Invalid Body", 422}
	}
	if requestBody.Lon == 0.0 {
		return &route.HttpError{errors.New("Long could not be found"), "Invalid Body", 422}
	}
	if requestBody.Gcm == "" {
		return &route.HttpError{errors.New("Gcm could not be found"), "Invalid Body", 422}
	}
	if err := service.Scan(&requestBody, 1); err != nil {
		return &route.HttpError{err, "Error scanning", 500}
	}
	return nil
}
