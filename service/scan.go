package service

import (
	"pokehelper/model"

	"github.com/pkmngo-odi/pogo/api"
)

func Scan(u *model.User, rings int) error {
	/*
		radius := config.Radius
		throttle := time.Tick(time.Second / 1)
	*/
	session, err := u.GetSession()
	if err != nil {
		return err
	}
	scan(session, u.GetLocation())
	// If all successful, add to JobQueue with current timestamp
	return nil
}

func scan(s *api.Session, l *api.Location) {
	s.MoveTo(l)
	s.Announce()
}
