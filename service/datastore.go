package service

import (
	"pokehelper/model"
)

func CreateUser(user *model.User) error {
	return nil
}

func GetUser(email string) (*model.User, error) {
	return &model.User{
		Email:    "next.generation2207@gmail.com",
		Password: "Zxzxzxz1",
		Lat:      151.0,
		Lon:      33.0,
		Gcm:      "ABC",
	}, nil
}

func UpdateLatLong(email string, lat, lon float64) error {
	user, err := GetUser(email)
	if err != nil {
		return err
	}
	user.Lat = lat
	user.Lon = lon

	return nil
}

func UpdateGcm(email, gcm string) error {
	user, err := GetUser(email)
	if err != nil {
		return err
	}
	user.Gcm = gcm

	return nil
}

func DeleteUser(email string) error {
	return nil
}
