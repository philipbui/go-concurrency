package route

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
)

type HttpError struct {
	Error   error
	Message string
	Code    int
}

func (e *HttpError) PrintError() string {
	return e.Message + ": " + e.Error.Error()
}

type Controller func(http.ResponseWriter, *http.Request, httprouter.Params) *HttpError

func ServeHTTP(fn Controller) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		if err := fn(w, r, ps); err != nil {
			http.Error(w, err.PrintError(), err.Code)
		}
	}
}
