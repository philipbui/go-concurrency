// pokehelper project main.go
package main

import (
	"fmt"
	"log"
	"net/http"
	"pokehelper/config"
	"pokehelper/controller"
	"pokehelper/route"
	"pokehelper/workerpool"
	"time"

	"github.com/julienschmidt/httprouter"
)

func Index(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	fmt.Fprint(w, "Welcome!\n")
}

func main() {
	dispatcher := workerpool.NewDispatcher(6)
	dispatcher.Start()
	workerpool.JobQueue = make(chan *workerpool.Job)
	workerpool.JobQueue <- &workerpool.Job{
		Email:     "abc",
		Timestamp: time.Now(),
	}
	router := httprouter.New()
	router.GET("/", Index)
	router.POST("/create-user", route.ServeHTTP(controller.CreateUser))
	router.POST("/get-user", route.ServeHTTP(controller.GetUser))
	router.POST("/delete-user", route.ServeHTTP(controller.DeleteUser))
	router.POST("/scan", route.ServeHTTP(controller.Scan))
	router.POST("/update-gcm", route.ServeHTTP(controller.UpdateGcm))
	router.POST("/update-latlng", route.ServeHTTP(controller.UpdateLatLng))
	log.Fatal(http.ListenAndServe(config.Port, router))
}
